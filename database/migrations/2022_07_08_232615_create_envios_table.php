<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envios', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('pocion_id')->unsigned();
            $table->bigInteger('cliente_id')->unsigned();
            $table->integer('cantidad');
            $table->dateTime('fecha');
            $table->timestamps();
            $table->double('precio_total')->nullable();
            $table->foreign('pocion_id')->references('id')->on('pocions')->onDelete('cascade');
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envios');
    }
};
