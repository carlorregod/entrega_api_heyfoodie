<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pocions_ingredientes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('pocion_id')->unsigned();
            $table->bigInteger('ingrediente_id')->unsigned();
            $table->float('cantidad');
            $table->integer('precio_unitario');
            $table->timestamps();
            $table->foreign('pocion_id')->references('id')->on('pocions')->onDelete('cascade');
            $table->foreign('ingrediente_id')->references('id')->on('ingredientes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pocions_ingredientes');
    }
};
