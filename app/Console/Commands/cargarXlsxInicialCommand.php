<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

//Fast excel
use Rap2hpoutre\FastExcel\FastExcel;

//Modelos
use App\Models\Pocion, App\Models\Envio, App\Models\Ingrediente, App\Models\Cliente;
//DB
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


class cargarXlsxInicialCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:dataxlsx';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ejecuta carga inicial de muestra de datos desde archivo storage/app/load/Data Evaluacion.xlsx';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            self::truncandoTablas();
        } catch (\Exception $e) {
            throw new \Exception("Error al truncar tablas, detalle: {$e->getMessage()}",1);
        }
        $collection_data = (new FastExcel)->withSheetsNames()->importSheets(storage_path('app/load/Data Evaluacion.xlsx'))->toArray();
        // LLenando 3 tablas
        foreach($collection_data['Pociones'] as $clave =>$pocion) {
            $potion = Pocion::where('nombre',$pocion['POCIONES'])->first();
            $ingrediente = Ingrediente::where('nombre',$pocion["INGREDIENTES"])->first();
            if(is_null($potion)) {
                $potion = $this->nuevapocion($pocion['POCIONES']);
            }
            if(is_null($ingrediente)) {
                $ingrediente = $this->nuevoingrediente($pocion["INGREDIENTES"]);
            }
            //Se crea la relación entre ellos
            $this->pocioningrediente($potion->id, $ingrediente->id, $pocion);
        }

        //LLenando tabla de ventas o envíos
        foreach($collection_data["Envios"] as $clave =>$envio) {
            $cliente = Cliente::where('nombre',$envio["cliente"])->first();
            if(is_null($cliente)) {
                $cliente = $this->nuevocliente($envio["cliente"]);
            }
            $potion = Pocion::where('nombre',$envio["poCion"])->first();
            $this->nuevoenvio($envio, $cliente->id, $potion);
        }

        echo "Fin script";

    }

    private function nuevapocion($p) :object {
        $pocima = new Pocion();
        $pocima->nombre = $p;
        $pocima->save();
        return $pocima;
    }
    private function nuevoingrediente($i) :object {
        $ingrediente = new Ingrediente();
        $ingrediente->nombre = $i;
        $ingrediente->save();
        return $ingrediente;
    }
    private function pocioningrediente($potion_id, $ingrediente_id, $p) :void {
        DB::table('pocions_ingredientes')->insert([
            'pocion_id'     =>$potion_id,
            'ingrediente_id'=>$ingrediente_id,
            'cantidad'=>$p["CANTIDAD"],
            'precio_unitario'=>$p["PRECIO ( 1 UN)"],
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

    }
    private function nuevocliente($c) :object {
        $cliente = new Cliente();
        $cliente->nombre = $c;
        $cliente->save();
        return $cliente;
    }
    private function nuevoenvio($envio, $cliente_id, $pocion): void {
        //Precálculo del precio total del brevaje
        $subtotal = self::totalventa($pocion); //Precio unitario de poción
        $total_venta = round($subtotal * $envio["cantidad"],2); //Redondeo a dos decimales
        $envio = Envio::create([
            'cantidad'=>$envio["cantidad"],
            'fecha'=>$envio["fecha"],
            'pocion_id'=>$pocion->id,
            'cliente_id'=>$cliente_id,
            'precio_total'=>$total_venta,
        ]);

    }
    //Precio total brevaje almacenado en BD, de uno solo
    private static function totalVenta($pocion) :int{
        $pocimaingredientes = DB::table('pocions_ingredientes')->where('pocion_id',$pocion->id)->get();
        $subtotal = 0;
        foreach($pocimaingredientes as $pi) {
            $subtotal += $pi->precio_unitario * $pi->cantidad;
        }
        return $subtotal;

    }

    //Truncando base de datos
    private static function truncandoTablas() : void {
        Schema::disableForeignKeyConstraints();
        DB::table('pocions_ingredientes')->delete();
        Envio::truncate();
        Cliente::truncate();
        Pocion::truncate();
        Ingrediente::truncate();
        Schema::enableForeignKeyConstraints();

    }
}
