<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\VentaPorBrujaRequest;
use App\Http\Requests\UsoInsumosRequest;

use App\Http\Resources\UsoInsumosResource;

use Illuminate\Http\Request;

use App\Models\Envio;
use App\Models\Pocion;
use App\Models\Cliente;
use App\Models\Ingrediente;
use Illuminate\Support\Facades\DB;

class ReporteController extends Controller
{
    /**
     * Reporte de ventas por bruja: indicar cuantas pociones a comprado una o varias brujas en
        un periodo de tiempo, costo de las mismas en unidad y compra total (POST enviando id
        brujas y rango de fechas, en caso de no especificar bruja entregar todas) (bruja, cantidad
        comprada por poción, total consolidado compras)

     */
    public function ventaPorBruja(VentaPorBrujaRequest $request)
    {
        $venta = Envio::whereBetween('fecha',[$request->fecha_inicio, $request->fecha_final]);

        if(isset($request->cliente_id)) {
            $venta = $venta->where('cliente_id',$request->cliente_id);
        }

        $venta = $venta
            ->selectRaw("cliente_id, pocion_id, sum(precio_total) as venta_total, count(*) as venta_cantidad")
            ->groupBy(['cliente_id','pocion_id'])
            ->orderBy('cliente_id')
            ->get()->toArray();

        $report = array();
        foreach($venta as $clave => $v) {
            // $bruja = $v['cliente_id']; //Puede customizarse con el nombre como en la linea sgte
            $bruja = Cliente::where('id',$v['cliente_id'])->pluck('nombre')->first();
            $report[$bruja][] = [
                'pocion'=>Pocion::where('id',$v['pocion_id'])->pluck('nombre')->first(),
                'pocion_id'=>$v['pocion_id'],
                'cantidad'=>$v['venta_cantidad'],
                'venta_total'=>"$".$v['venta_total'],

            ];
        }

        return response()->json(['message'=>'Exito','data'=>$report]);

    }

    /**
     * Reporte de uso de insumos (ingredientes) en un periodo de tiempo retornando
        información ingrediente, costo unitario y costo total (POST enviando rango de fechas y
        id pociones, en caso de no especificar poción traer info de todas)
     */
    public function usoInsumos(UsoInsumosRequest $request)
    {
        //Asumiendo que el uso de insumos es proporcional a los envios...
        $venta = Envio::whereBetween('fecha',[$request->fecha_inicio, $request->fecha_final]);
        if(isset($request->pocion_id)) {
            $venta = $venta->where('pocion_id',$request->pocion_id);
        }

        $venta = $venta
            ->selectRaw("pocion_id, sum(precio_total) as venta_total, count(*) as venta_cantidad")
            ->groupBy('pocion_id')
            ->orderBy('pocion_id')
            ->get()->toArray();

        // dd($venta);
        $reporte = array();

        foreach($venta as $clave => $v) {
            // $pocima = $v['pocion_id']; //Puede customizarse con el nombre como en la linea sgte
            $pocima = Pocion::find($v['pocion_id']);
            $ingredientes = $pocima->ingredientes()->pluck('nombre','ingredientes.id')->toArray();
            // Complementando...
            foreach($ingredientes as $clave => $ingrediente) {
                $ing = DB::table('pocions_ingredientes')->where('pocion_id',$v['pocion_id'])->where('ingrediente_id',$clave)
                    ->select('cantidad','precio_unitario')->first();

                $ingredientes[$clave] = [
                    'nombre'=>$ingredientes[$clave],
                    // 'cantidad_utilizada_unitaria'=>$ing->cantidad,
                    // 'venta_ingrediente_unitaria'=>$ing->precio_unitario,
                    'cantidad_utilizada_total'=>$ing->cantidad * $v['venta_cantidad'],
                    'venta_ingrediente_total'=> $ing->precio_unitario * $ing->cantidad * $v['venta_cantidad'],
                ];

            }
            // dd($ingredientes);
            $reporte[$pocima->id] = [
                'pocion'=>$pocima->nombre,
                // 'cantidad'=>$v['venta_cantidad'],
                // 'coste'=>$v['venta_total'],
                'ingredientes_utilizados'=>$ingredientes,
            ];

        }

        return response()->json(['message'=>'Exito','data'=>$reporte]);
    }
}
