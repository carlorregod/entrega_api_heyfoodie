<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use App\Models\User;

use Illuminate\Support\Facades\Artisan;

class UserController extends Controller
{
    public function register(Request $request) {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            "status" => 1,
            "msg" => "¡Registro de usuario exitoso!",
        ]);
    }

    public function getToken(Request $request) {
        $seconds = config('sanctum.expiration');

        $request->validate([
            "email" => "required|email",
            "password" => "required"
        ]);

        $user = User::where("email", "=", $request->email)->first();

        if( isset($user->id) ){
            if(Hash::check($request->password, $user->password)){
                //creamos el token
                $token = $user->createToken("auth_token")->plainTextToken;
                //si está todo ok
                if(is_null(config('sanctum.expiration'))){
                    $expiration = 'indefinido';
                }else{
                    $expiration = now()->addSeconds($seconds)->format('Y-m-d H:i:s');; //Se agerega el tiempo para mostrarlo al cliente
                }
                return response()->json([
                    "status" => 1,
                    "msg" => "¡Usuario logueado exitosamente!",
                    "access_token" => $token,
                    "token_expiration" => $expiration,
                ]);
            }else{
                return response()->json([
                    "status" => 0,
                    "msg" => "La password es incorrecta",
                ], 404);
            }

        }else{
            return response()->json([
                "status" => 0,
                "msg" => "Usuario no registrado",
            ], 404);
        }
    }

    public function resetdata() {
        Artisan::call('load:dataxlsx');
        return response()->json(['message'=>'La data ha sido reiniciada a los valores por defecto. Los cambios anteriores y CRUDs efectuados han sido eliminados.']);
    }

}
