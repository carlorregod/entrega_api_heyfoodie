<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Http\Requests\StoreClienteRequest;
use App\Http\Requests\UpdateClienteRequest;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Cliente::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreClienteRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClienteRequest $request)
    {
        $cliente = Cliente::where("nombre",$request->nombre)->first();
        if(is_null($cliente)) {
            $cliente = new Cliente;
            $cliente->nombre = $request->nombre;
            $cliente->save();
            return response()->json(["message"=>"Exito","data"=>$cliente]);

        } else {
            return response()->json(['message'=>"Ya existe ese cliente, no se crer nuevo","data"=>[]]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::find($id);
        if(is_null($cliente)) {
            return response()->json(['mensaje'=>'id inválido','data'=>[]]);
        } else {
            return response()->json(['mensaje'=>'Exito','data'=>$cliente]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateClienteRequest  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClienteRequest $request, $id)
    {
        $cliente = Cliente::find($id);
        if(is_null($cliente)) {
            return response()->json(['message'=>"Ingrese un id que corresponda o que exista en los datos","data"=>[]]);
        } else {
            $cliente->nombre = $request->nombre;
            $cliente->save();
            return response()->json(["message"=>"Exito","data"=>$cliente]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::find($id);
        $nombre = $cliente->nombre;
        try {
            $cliente->delete();
            return response()->json(["message"=>"Exito, cliente {$nombre} ha sido eliminado","error"=>""]);
        } catch (\Exception $e) {
            return response()->json(["message"=>"Error, cliente {$nombre} NO fue eliminado,","error"=>$e->getMessage()],400);
        }

    }


}
