<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Envio extends Model
{
    use HasFactory;

    protected $primaryKey='id'; //Definición de primary key
    protected $foreignKey=[
        'pocion_id',
        'cliente_id',
    ]; //Definición de claves foráneas


    protected $fillable = [
        'cantidad',
        'fecha',
        'pocion_id',
        'cliente_id',
        'precio_total'
    ];


}
