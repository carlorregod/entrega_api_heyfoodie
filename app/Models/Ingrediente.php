<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    use HasFactory;

    public function pocion()
    {
        return $this->belongsToMany('App\Models\Pocion','pocions_ingredientes'); //”Tiene algunos…”
    }
}
