<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pocion extends Model
{
    use HasFactory;

    //Completamos la relación con los ingredientes
    public function ingredientes()
    {
        return $this->belongsToMany('App\Models\Ingrediente','pocions_ingredientes'); //”Tiene algunos…”
    }

}
