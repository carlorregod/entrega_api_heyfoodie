# Alcances

Dentro de las posibilidades abiertas, se optará por un sistema de login con Laravel JetStream (incluye Laravel Sanctum).

* Uso Laravel 9
* Versión de node.js utilizada: 16.13.1


+ [Vuelva a la pantalla principal](../README.md).

## Preparación propia
<pre>
composer require laravel/jetstream
php artisan jetstream:install livewire --teams
composer require nascent-africa/jetstrap --dev
php artisan jetstrap:swap livewire --teams
npm install && npm run dev
php artisan migrate
</pre>

También se ocupará
<pre>
composer require rap2hpoutre/fast-excel
</pre>

La compilación de npm es opcional porque la prueba está enfocada a las APIs.

## Otros

* Los reportes fueron creados como API REST como dice el enunciado.
* El recurso elegido para el CRUD es la tabla (entidad) de "clientes" (las brujas).
* Acá el MER del proyecto:

![MER](MER.png)

