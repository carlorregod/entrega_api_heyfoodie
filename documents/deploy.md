# Despliegue

+ [Vuelva a la pantalla principal](../README.md).

## Instrucciones

Ejecutar comandos
<pre>
git clone https://gitlab.com/carlorregod/entrega_api_heyfoodie.git pociones
cd pociones
composer install
php artisan key:generate
php artisan migrate
</pre>

Recuerde que puede testear con php artisan serve. 

Es importante mencionar que se ha proporcionado un mecanismo para establecer la caducidad del token. Éste puede ser agregado a su archivo **.env** para agregarlo como (ejemplo para una hora=3600 segundos):

<pre>
TOKEN_EXPIRATION = 3600
</pre>

Para poblar la base de datos

<pre>
{{URL}}/api/resetdata
</pre>

Donde **{{URL}}** es la url de su dominio en donde se ha desplegado el sistema. En caso que se despliegue con docker considerar habilitar conexiones con mysql.

Si no se especifica, por defecto el token caducará en 3 horas desde su creación.

## Poblar la base de datos

Al levantar el servidor, puede efectuar aquello de dos formas, el servidor debe estar arrriba ya sea con php artisans serve o la forma seleccionada.

a) A través de comando
<pre>
php artisan load:dataxlsx
</pre>

b) Utilizando la API
<pre>
{{URL}}/api/resetdata (GET)
</pre>

[Recomiendo consultar las APIs documentadasS en Postman](https://documenter.getpostman.com/view/18740589/UzJPMb3M)
