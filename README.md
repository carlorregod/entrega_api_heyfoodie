<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

# Entrega de prueba técnica HeyFoodie

## Documentación
* [Publicación en Postman](https://documenter.getpostman.com/view/18740589/UzJPMb3M)
* O Puede descargar el recurso para usarlo en Postman a través de [este enlace](documents/HeyFoodie.postman_collection.json)

* Alcances tomados para el mismo [puede revisarlos acá](documents/alcances.md).

* Despliegue [puede revisarlo acá](documents/deploy.md).

* El enunciado del ejercicio se encuentra [en este enlace](documents/enunciado.md).
